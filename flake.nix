{
  description = "Packaging script to deploy wiki";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
    poetry.url = "github:nix-community/poetry2nix";
    };

  outputs = inputs@{ self, nixpkgs, flake-utils, poetry }:
    let overlay = final: prev: ({
        wikiApp = final.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
          python = prev.python39;
          propagatedBuildInputs = [
            prev.graphviz
            prev.bash
            prev.wget
            prev.findutils
          ];
        };
    } //
    (poetry.overlay final prev));
    in
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; overlays = [ overlay]; };
      in  {
        defaultPackage =  pkgs.wikiApp;
        }) // {
      inherit overlay;
    };
}
