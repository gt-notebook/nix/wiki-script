import click

from .preprocess import commands as group1
from .build import commands as group2
from dotenv import load_dotenv

load_dotenv()

@click.group()
def entry_point():
    pass

entry_point.add_command(group1.process)
entry_point.add_command(group2.build)

if __name__ == '__main__':
    entry_point()