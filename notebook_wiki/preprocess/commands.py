import click
from . import convert
from . import retrievebib
from pathlib import Path
import shutil
import subprocess
import pkgutil

@click.command()
@click.argument('input', type=click.Path(exists=True, path_type=Path))
def process(input):

    output_name = input.name + "_converted"
    output = Path(input.parent / output_name)
    output.mkdir(parents=True, exist_ok = True)
    subprocess.run(["slipbox", "init", "--directory", output])

    # prepare cfg files
    root = Path.cwd()
    configfile = pkgutil.get_data('notebook_wiki', 'config.cfg').decode()
    config_dest = Path( output / '.slipbox' / 'config.cfg')
    config_dest.touch()
    config_dest.write_text(configfile)
    #shutil.copy(config_src, config_dest)

    # prepare resources
    img_src = Path(input / 'images')
    img_dest = Path(output / 'images')
    shutil.copytree(img_src, img_dest, dirs_exist_ok=True)


    convert.duplicate_files(input,output)
    convert.convert(output)
    retrievebib.download(output)





