
from pathlib import Path
from pyzotero import zotero
import bibtexparser
import os

def download(output):

    GROUP_ID = os.getenv('GROUP_ID')
    ZOTERO_KEY = os.getenv('ZOTERO_KEY')

    dest_file = Path(output / 'notebooks.bib')

    zot = zotero.Zotero(GROUP_ID, 'group', ZOTERO_KEY)
    zot.add_parameters(format='bibtex',style='mla')

    result = zot.items()

    with open(dest_file, 'w') as bibtex_file:
        bibtexparser.dump(result, bibtex_file, writer=None)

    print("download bibtex into" , dest_file)