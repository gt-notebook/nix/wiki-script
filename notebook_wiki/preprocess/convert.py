import subprocess
from pathlib import Path
import re
from ruamel.yaml import YAML

def get_metadata(src):
     yaml_str = src.lstrip().split('---\n', maxsplit=2)
     yaml_str = yaml_str[1]   # re-add the trailing newline that was split off
     yaml = YAML(typ='safe')
     data = yaml.load(yaml_str)
     return data

def new_metadata(src, stem, erase):

     with open(src, 'r+') as f:
          content = f.read()
          metadata = get_metadata(content)

          # remove all
          if erase :
            text = re.sub(r'^\s*---(.*)---\s*$','', content, flags= re.MULTILINE | re.DOTALL)

          f.seek(0)
          text = '# {thestem} {title}\n'.format(thestem= stem, title = metadata["title"]) + text
          f.write(text)
          f.truncate()

     return metadata

def regex_wikilinks(src, d_metadata) :
     with open(src, 'r+') as f:
          content = f.read()
          # previous regex not good \[\[([^/]+)\]\]
          text = re.sub(r'\[\[(\w*-*\w*)\]\]',lambda m: create_wikilinks(m.group(), d_metadata), content, flags = re.M)
          f.seek(0)
          f.write(text)
          f.truncate()

def create_wikilinks(src, d_metadata):
     link_stripped = src.strip("[]")
     link = '[{title}](#{link})'.format(title = d_metadata[link_stripped]["title"], link = link_stripped )
     return link

def markdown_files(src):
    return src.glob('**/*.md')

def replace_metadata(input,erase=True):
    d_metadata = {}
    for file in markdown_files(input):
        print("process (and erase) metadata - {file}".format(file=file.stem))
        metadata =  new_metadata(file, file.stem, erase)
        d_metadata[file.stem] = metadata
        print("metadata = {md}".format(md=metadata))
    return d_metadata

def replace_wikilinks(input,metadatas):
    for file in markdown_files(input):
        print("replace links - {file}".format(file=file.stem))
        regex_wikilinks(file, metadatas)

def duplicate_files(input, output):
    for file in markdown_files(input):
        dest = Path(output / file.name)
        print("duplicate - {file}".format(file=file.stem))
        dest.touch()
        # Overwrite existing
        dest.write_bytes(file.read_bytes())
        print("*********")

def convert(input):
    metadata = replace_metadata(input, True)
    replace_wikilinks(input, metadata)
