import click
import subprocess
from pathlib import Path

@click.command()
@click.argument('input', type=click.Path(exists=True, path_type=Path))
def build(input):
    subprocess.run(["pandoc","-v"])
    subprocess.run(["slipbox", "build"], cwd=input)